class HelloButtonProperty extends HTMLElement {
  constructor() {
    super();

    let text = this.getAttribute('text');
    let message = this.getAttribute('message');

    this.innerHTML = '<button>' + text + '</button>';
    const button = this.querySelector('button');

    button.addEventListener('click', function(e) {
      alert(e.target.parentNode.getAttribute('message'))
    });
  }
}

customElements.define('hello-button-property', HelloButtonProperty);