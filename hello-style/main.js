class HelloStyle extends HTMLElement {
  constructor() {
    super();

    let color = this.getAttribute('color') || '';

    this.innerHTML = '<p>Hello Style!</p>';
    const ptag = this.querySelector('p');
    ptag.style.color = color;
  }
}

customElements.define('hello-style', HelloStyle);