class HelloWorld extends HTMLElement {
  constructor() {
    super();

    this.innerHTML = '<p>Hello World!</p>';
  }
}

customElements.define('hello-world', HelloWorld);