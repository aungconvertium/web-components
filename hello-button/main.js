class HelloButton extends HTMLElement {
  constructor() {
    super();

    this.innerHTML = '<button>Hello</button>';
    const button = this.querySelector('button');
    button.addEventListener('click', this.handleClick);
  }

  handleClick(e) {
    alert('Hello World!');
  }
}

customElements.define('hello-button', HelloButton);